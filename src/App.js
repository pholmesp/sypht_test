import React, {useEffect, useState} from "react";
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import { Collapse } from 'antd';
import { Spin, Icon } from 'antd';
import './App.css'
import 'antd/dist/antd.css';
import {BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';


const { Panel } = Collapse;
const antIcon = <Icon type="loading" style={{ fontSize: 24 }} spin />;

const useStyles = makeStyles(theme => ({
    button: {
        margin: theme.spacing(1),
        borderRadius: 20,
    },
    input: {
        display: "none"
    }
}));

export default function OutlinedButtons() {
    const classes = useStyles();
    const [files, setFiles] = useState('');
    const [value, setValue] = React.useState('');
    const [tokenBilles, setTokenBilles] = React.useState(null);
    const [tokenGenerics, setTokenGenerics] = React.useState(null);
    const [type, setType] = React.useState(null);
    const [result, setResult] = React.useState(null);
    const [spin, setSpin] = React.useState(false);
    const [chartData, setChartData] = React.useState([]);
    const [abn, setABN] = React.useState(null);
    const uploadUrl = 'https://api.sypht.com/fileupload'
    const authorizationUrl = 'https://login.sypht.com/oauth/token'
    const resultUrl = 'https://api.sypht.com/result/final/'


    useEffect(()=>{
        authorizationBills()
        authorizationGeneric()
    }, [])

    const authorizationBills = () => {
        fetch(authorizationUrl, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "client_id": 'HOAJaSH5HyLSmVUQrZ7DtfI6rLb7tlAe',
                "client_secret": '20wMH-PATNXXsvvZ09q37Ufo4dZsbuHfvIlygRr7dep-YikG1SHvYQGAYBMgaESd',
                "audience": "https://api.sypht.com",
                "grant_type": "client_credentials",
            })
        }).then(res => {
            if (res.status === 200) {
                return res.json();
            }
        }).then(json => {
            setTokenBilles(json.access_token)
        })
    }

    const authorizationGeneric = () => {
        fetch(authorizationUrl, {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                "client_id": '3xKDdZOwLNpcIu7SjuEKwrmQ0eL14fM5',
                "client_secret": 'PGzJKLdtBx6zGluuybd8nGpHqMBiWkJV8Or7lx6QXbanbfcYDRBnG3JmGLYZks4S',
                "audience": "https://api.sypht.com",
                "grant_type": "client_credentials",
            })
        }).then(res => {
            if (res.status && res.status === 200) {
                return res.json();
            }
        }).then(json => {
            setTokenGenerics(json.access_token)
        })
    }

    const fetchResult = (fileId, value) => {

        const bearer = type==='document'||'invoice'? tokenBilles: tokenGenerics
        fetch(resultUrl+fileId.toString(), {
            method: "GET",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': "Bearer " + bearer
            },
        }).then(res => {
            if (res.status && res.status === 200) {
                return res.json();
            }
        }).then(json => {
            console.log(json)
            if(json.status && json.status==='FINALISED'){
                let date, val
                if(value==='invoice'){
                    json.results.fields.map(field=>{
                        if (field.name ==='invoice.dueDate'){
                            date = field.value
                        }
                        else if (field.name ==='invoice.total'){
                            val = field.value
                        }
                    })
                    let tempData = {'Date': date, 'Value': val}
                    setChartData([...chartData, tempData])
                }
                else if(value ==='document'){
                    console.log(json.results.fields[0].value)
                    json.results.fields.map(field=>{
                        if (field.name ==='document.supplierABN'){
                            verifyABN(field.value.toString())
                            return
                        }
                    })

                }
                setResult(json.results)

            }
            setSpin(false)
        })
    }

    const onChangeHandler = event => {
        console.log(event.target.files);
        setFiles(event.target.files[0])
        setResult(null)
    };
    const handleRadioChange = event => {
        setValue(event.target.value);
    };

    const verifyABN = (abn) =>{
        console.log(typeof abn)
        console.log(abn)
        setABN(`https://abr.business.gov.au/abrxmlsearch/AbrXmlSearch.asmx/SearchByABNv201408?searchString=${abn}&includeHistoricalDetails=N&authenticationGuid=4af9e61b-3752-4846-a8b2-4577994d75aa`)
    }

    const onSubmitHandler = () => {
        if (!value) {
          return alert("Please choose a type !")
        }
        if (!tokenBilles || !tokenGenerics){
          return alert("Not yet registered, Please click register button first !")
        }
        if (!files || files.length<1 ) {
          return alert("Please upload a file !")
        }
        if(value !== 'invoice'){
            setChartData([])
        }
        if(value !== 'document'){
            setABN(null)
        }
        setSpin(true)
        setResult(null)
        setType(value)
        const bearer = value==='document'||'invoice'? tokenBilles: tokenGenerics
        var data = new FormData();
        data.append("fileToUpload", files);
        data.append("fieldSet", `sypht.${value}`)

        fetch(uploadUrl, {
            method: "POST",
            mode: 'cors',
            headers: {
                'Accept': "application/json",
                //"Content-Type": "multipart/form-data",
                'Authorization': "Bearer " + bearer
            },
            body: data
        })
            .then(res => {
                if (res.status === 200) {
                    return res.json();
                }
            })
            .then(json => {
                console.log(json);
                if( json.status && json.status ==='RECEIVED'){
                    fetchResult(json.fileId, value)
                }
            });
    };

    return (
        <div className="OutsideContainer">
            <input
                //accept="image/*"
                className={classes.input}
                id="outlined-button-file"
                //multiple
                type="file"
                onChange={onChangeHandler}
            />
            <div>
                <p>Hi, you are welcome to upload files here, please select the file type before you submit.</p>
            </div>

            <div>
                <FormControl component="fieldset">
                    <RadioGroup aria-label="position" name="position" value={value} onChange={handleRadioChange} row>
                        <FormControlLabel
                            value="document"
                            control={<Radio color="primary" />}
                            label="Document"
                            labelPlacement="bottom"
                        />
                        <FormControlLabel
                            value="invoice"
                            control={<Radio color="primary" />}
                            label="Receipt / Invoice"
                            labelPlacement="bottom"
                        />
                        <FormControlLabel
                            value="generic"
                            control={<Radio color="primary" />}
                            label="Generic"
                            labelPlacement="bottom"
                        />
                    </RadioGroup>
                </FormControl>
            </div>
            <div className="WrapDiv">

                <label htmlFor="outlined-button-file">
                    <Button variant="outlined" component="span" className={classes.button}>
                        Upload
                    </Button>
                </label>
            </div>
            <div>
                { files? <span>{files.name} has been uploaded.</span>: <br />}
            </div>
            <div className='submitButton'>
                <Button
                    variant="contained"
                    color="primary"
                    //onSubmitHandler
                    onClick={onSubmitHandler}
                    component="span"
                    className={classes.button}
                >
                    submit
                </Button>
            </div>
            <div>
                <Spin indicator={antIcon} spinning={spin}/>
            </div>
            { abn? <>Please click following url to verify ABN on document: <a href={abn}>ABN Verification Result</a></>:null}
            <div className='resultDiv'>
                {chartData.length>0 ? <BarChart width={600} height={300} data={chartData}
                          margin={{top: 5, right: 30, left: 20, bottom: 5}}>
                    <CartesianGrid strokeDasharray="3 3"/>
                    <XAxis dataKey="Date"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend />
                    <Bar dataKey="Value" fill="#8884d8" />
                </BarChart>: null}
                <Collapse defaultActiveKey={['1']}>
                    {
                        type !== 'generic' && result? result.fields.map(field => {
                            //console.log(typeof field.value === "object" && field.value != null)
                            return (
                                <Panel header={field.name} key={field.confidence}>
                                    {typeof field.value === "object" && field.value != null ?

                                        <ul>
                                            {Object.keys(field.value).map(key =>
                                                <li key={key}>
                                                    {key}: {field.value[key].value === null ? 'Null' : field.value[key].value.toString()}, <span
                                                    style={{color: 'green'}}>Confidence</span> : {field.value[key].confidence}
                                                </li>
                                            )}

                                        </ul>
                                        : <span>Value: {field.value === null ? 'Null' : field.value}, <span
                                            style={{color: 'green'}}> Confidence: </span>{field.confidence}</span>
                                    }
                                </Panel>
                            )
                        })
                        :
                            type === 'generic' && result? result.fields.map(field =>
                            (<Collapse key='5'>
                                        <Panel header='Entities' key='1'>
                                            <ul>
                                         {Object.keys(field.value.entities).length && Object.keys(field.value.entities).map(key=>
                                             <li key={key}>
                                                 {field.value.entities[key].text}
                                             </li>
                                         )}
                                            </ul>
                                        </Panel>
                                    <Panel header='Fields' key='2'>
                                        <ul>
                                            {field.value.fields.length && field.value.fields.map((key, idx)=>
                                                <li key={idx}>
                                                    <span style={{ color: 'green'}}>{key.field_key}:</span> {key.value.substring(17)}
                                                </li>
                                            )}
                                        </ul>
                                    </Panel>
                                    <Panel header='Relations' key='3'>
                                        <ul>
                                            {field.value.relations.length && field.value.relations.map((key, idx)=>
                                                <li key={idx}>
                                                    <span style={{ color: 'green'}}>Type:</span> {key.type}, <span style={{ color: 'green'}}>Parent ID:</span> {key.parent_id}, <span style={{ color: 'green'}}>Child ID:</span> {key.child_id}
                                                </li>
                                            )}
                                        </ul>
                                    </Panel>

                                </Collapse>
                            )
                        ) : null
                    }
                </Collapse>
            </div>
        </div>
    );
}

